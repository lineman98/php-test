<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\News\NewsService;

class NewsParse extends Command
{
    protected $newsService = null;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update news from rbc.ru';

    /**
     * NewsParse constructor.
     *
     * @param NewsService $newsService
     */
    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->newsService->sync();

        return 0;
    }
}
