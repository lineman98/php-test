<?php

namespace App\Http\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->get('id'),
            'title' => $this->get('title'),
            'description' => $this->get('description'),
            'source_url' => $this->get('source_url'),
            'picture_url' => $this->get('picture_url') ?? null,
            'picture_title' => $this->get('picture_title') ?? null
        ];
    }
}
