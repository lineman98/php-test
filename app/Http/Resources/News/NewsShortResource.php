<?php

namespace App\Http\Resources\News;

use Illuminate\Http\Resources\Json\JsonResource;

class NewsShortResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->get('id'),
            'title' => $this->get('title'),
            'description' => $this->getDescription(),
            'url' => $this->get('source_url')
        ];
    }

    protected function getDescription() {
        return substr($this->get('description') ?? '', 0, 200);
    }
}
