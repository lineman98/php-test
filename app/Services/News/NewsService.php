<?php
namespace App\Services\News;

use App\Models\News;
use App\Services\Parser\Parser;
use App\Services\Parser\TagAttribute;
use App\Services\Parser\TagClass;

class NewsService {
    protected $parser;
    protected $baseUrl = 'https://www.rbc.ru';

    public function __construct() {
        $this->parser = app(Parser::class);
    }

    public function getAll() {

    }

    /**
     * Запускает парсинг и обноваляет данные в бд
     */
    public function sync(): void {
        $news = $this->parser
            ->setBaseUrl($this->baseUrl)
            ->parse('.news-feed')
            ->parse('.js-news-feed-list')
            ->parse('.news-feed__item')
            ->get([
                'source_url' => new TagAttribute('href'),
                'title' => new TagClass('news-feed__item__title'),
                'category' => new TagClass('news-feed__item__date-text')
            ]);

        foreach($news as $article) {
            $article = new News($article);

            $fullArticleArray = $this->parser->reset()
                ->setBaseUrl($article->source_url)
                ->parse('.article .article__content')
                ->get([
                    'title' => new TagClass('js-slide-title'),
                    'description' => new TagClass('article__text'),
                ]);
        }
    }

}
