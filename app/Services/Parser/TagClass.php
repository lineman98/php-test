<?php
namespace App\Services\Parser;

class TagClass
{
    protected $name = '';

    public function __construct(string $name) {
        $this->name = $name;
    }

    public function name() {
        return '.' . $this->name;
    }
}
