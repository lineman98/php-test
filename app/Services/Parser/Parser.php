<?php
namespace App\Services\Parser;

use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;

class Parser {
    protected $baseUrl = null;
    protected $parseSequence = [];

    private $value = [];

    public function __construct() {

    }

    /**
     * @param string $url
     * @return $this|object
     */
    public function setBaseUrl(string $url): object {
        $this->baseUrl = $url;

        return $this;
    }

    /**
     * @param $pattern
     * @return $this|object
     */
    public function parse($pattern): object {
        $this->parseSequence[] = $pattern;

        return $this;
    }

    public function get(array $fields) {
        $this->value = [ new Crawler($this->getHttpContent()) ];

        $this->executeParseSequence();
        $results = $this->extractResults($fields);

        return $results;
    }

    private function executeParseSequence() {
        foreach($this->parseSequence as $num => $parsePattern) {
            $newVal = [];
            foreach($this->value as $val) {
                $newVal = array_merge($newVal, $val->each(function ($node, $i) use ($parsePattern) {
                    return $node->filter($parsePattern);
                }));
            }
            $this->value = $newVal;
        }
    }

    public function reset() {
        $this->baseUrl = '';
        $this->parseSequence = [];
        $this->value = [];

        return $this;
    }

    private function extractResults(array $fields) {
        $result = [];
        foreach($this->value as $val) {
            $result = array_merge($result, $val->each(function($val) use ($fields, $result) {
                $extracted = [];
                foreach($fields as $fieldName => $field) {
                    if($field instanceof TagAttribute) {
                        $extracted[$fieldName] = $val->extract([$field->name()])[0] ?? '';
                    }
                    if($field instanceof TagClass) {
                        try {
                            $extracted[$fieldName] = $val->filter($field->name())->text();
                        } catch(\Exception $e) {
                            dd($e->getMessage(), $this->value);
                        }
                    }
                }
                return $extracted;
            }));
        }
        return $result;
    }

    /**
     * @return string
     */
    private function getHttpContent(): string {
        return Http::get($this->baseUrl);
    }
}
